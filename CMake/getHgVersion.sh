#!/bin/sh

SOURCE_DIR=$1
BUILD_DIR=$2

cd $SOURCE_DIR

echo ""
echo "const char *DATE_COMPILED = \"`date`\";"

if [ "$#" -gt 2 ]; then
  HG=$3

  echo "const char *HG_CHANGESET = \"`$HG identify -in`\";"
  echo "const char *HG_BRANCH = \"`$HG branch`\";"
  echo ""
  $HG diff > $BUILD_DIR/HG_PATCH
  cd $BUILD_DIR
  if [ `cat HG_PATCH | wc -w` -gt 0 ] ; then
    xxd -i HG_PATCH
  else
    echo "unsigned char HG_PATCH[] = {};"
    echo "unsigned int HG_PATCH_len = 0;" 
  fi

else
  echo "const char *HG_CHANGESET = \"\";"
  echo "const char *HG_BRANCH = \"\";"
  echo ""
  echo "unsigned char HG_PATCH[] = {};"
  echo "unsigned int HG_PATCH_len = 0;" 
fi

echo ""


