//
// Created by Goéric Daeninck on 2019-05-10.
//

#ifndef SOURCE_EIGENMODE_H
#define SOURCE_EIGENMODE_H

#include <map>
#include <vector>
#include <string>
#include <Eigen/Dense>

using Vector6d = Eigen::Matrix<double, 1, 6>;

struct EigenMode {
  int number = -1;

  double eigenvalue = 0.0;

  double cyclevalue = 0.0;

  EigenMode(int number = -1, double eigenvalue = 0.0, double cyclevalue = 0.0) :
    number(number), eigenvalue(eigenvalue), cyclevalue(cyclevalue) {}

  std::map<int, Vector6d> data;

  void print() const;

  static std::vector<EigenMode> parseNastranDumpFile(const std::string &filename);

  static std::vector<EigenMode> parseXPostFile(const std::string &filename);

  static std::vector<EigenMode> parsePchFile(const std::string &filename);
};


#endif //SOURCE_EIGENMODE_H
