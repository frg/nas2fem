//
// Created by Goéric Daeninck on 2019-05-02.
//

#ifndef SOURCE_DATACONTAINER_H
#define SOURCE_DATACONTAINER_H

#include "Tools/Exception.h"

template<typename DataType>
class DataContainer {

  struct IndexData {
    int index;
    int renumId;
    DataType data;
  };

  int maxRenumId = -1;
  int maxGlobalId = -1;

  std::map<int, IndexData> globalIdToData;
  std::vector<int> idxToGlobalId;

  bool renumber = false;
  int offset = 0;

  std::string name;

public:

  DataContainer(std::string name) : name(name) {}

  void setOffset(int o) { offset = o; }

  void setRenumbering(bool r) { renumber = r; }

  int insert(int gid, const DataType &data) {

    int globalId(gid);

    if (globalId < 1) {
      throw Exception(" ### PF.ERR: Index ", (int) globalId, ", is zero or negative in '", name, "'!");
    }

    int index = globalIdToData.size();
    int renumId = renumber ? (index + offset + 1) : (gid + offset);
    IndexData indexData = {index, renumId, data};

    auto itAndSuccess = globalIdToData.insert({globalId, indexData});
    if (itAndSuccess.second == false) {
      throw Exception(" ### PF.ERR: Index ", gid, " already exists in '", name, "'!");
    }

    idxToGlobalId.push_back(globalId);
    if (renumId > maxRenumId)
      maxRenumId = renumId;
    if (globalId > maxGlobalId)
      maxGlobalId = globalId;

    if ( globalIdToData.size() != idxToGlobalId.size() ) {
      throw Exception(" ### PF.ERR: DataContainer '", name, "' has inconsistent data (#globalToLocal = ", globalIdToData.size(),", #idxToGlobalId = ", idxToGlobalId.size(),")!");
    }

    return index;
  }

  int size() const {
    return globalIdToData.size();
  }

  bool contains(int gid) const {
    return globalIdToData.find(gid) != globalIdToData.end();
  }

  const DataType &operator[](int gid) const {
    auto it = globalIdToData.find(gid);
    if (it == globalIdToData.end() )
      throw Exception(" ### PF.ERR: Could not find data in '", name, "' for Index ", gid);
    return it->second.data;
  }

  DataType &operator()(int gid) {
    auto it = globalIdToData.find(gid);
    if (it == globalIdToData.end() )
      throw Exception(" ### PF.ERR: Could not find data in '", name, "' for Index ", gid);
    return it->second.data;
  }

  int indexToGid(int index) const {
    return (int) idxToGlobalId[index];
  }

  int indexToRenumGid(int index) const {
    int gid = indexToGid(index);
    auto it = globalIdToData.find(gid);
    if (it == globalIdToData.end() )
      throw Exception(" ### PF.ERR: Could not find data in '", name, "' for Index ", gid);
    return (int) it->second.renumId;
  }

  int renumGid(int gid) const {
    auto it = globalIdToData.find(gid);
    if (it == globalIdToData.end() )
      throw Exception(" ### PF.ERR: Could not find data in '", name, "' for Index ", gid);
    return (int) it->second.renumId;
  }

  int renumGidInternal(int gid) const {
    int internalOffset = gid - maxGlobalId;
    if (internalOffset < 1)
      throw Exception(" ### PF.ERR: renumGidInternal leads to negative or zero internal offset in '", name, "' for Index ", gid);
    return maxRenumId + internalOffset;
  }

  int renumGidAuto(int gid) const {
    int internalOffset = gid - maxGlobalId;
    if (internalOffset < 1)
      return renumGid(gid);
    else
      return maxRenumId + internalOffset;
  }

  int getMaxRenumGid() const {
    return maxRenumId;
  }

  int getMaxGid() const {
    return maxGlobalId;
  }

};

#endif //SOURCE_DATACONTAINER_H
