//
// Created by Michel Lesoinne on Apr 22, 2013.
//

#ifndef PMORSUITE_TOOLS_EXCEPTION_H
#define PMORSUITE_TOOLS_EXCEPTION_H

#include <string>
#include <sstream>
#include <exception>
#include <execinfo.h> // GCC and Clang

std::string cppDemangle(const char *abiName);

/** \brief Basic class to handle exceptions with a simple message */
class Exception : public std::exception {
  std::string reason;

  std::string bt;

  void append(std::stringstream &s) { }

  template <typename A, typename ...T> void append(std::stringstream &s, A a, T...t) {
    s << a;
    append(s, t...);
  }

public:

  template <typename ...T> Exception(const std::string &reason, T...t) {
    std::stringstream sReason;
    sReason << reason;
    append(sReason, t...);
	this->reason = sReason.str();
	std::stringstream sBT;
	void *trace[32];
	int trace_size = ::backtrace(trace, 32);
	char **traceText = backtrace_symbols(trace, trace_size);
	// Don't print the first trace, as it is this constructor.
	for (int i = 1; i < trace_size; ++i) {
	  sBT << cppDemangle(traceText[i]) << std::endl;
	}
	this->bt = sBT.str();
  }

  const char *what() const noexcept { return reason.c_str(); }

  const char *backtrace() const noexcept { return bt.c_str(); }

};

#endif // PMORSUITE_TOOLS_EXCEPTION_H
