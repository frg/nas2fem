//
// Created by Goéric Daeninck on 6/8/18.
//

#ifndef GLOBALID_H
#define GLOBALID_H

#include <vector>
#include <map>

class GlobalId {
protected:
  int v;

public:

  GlobalId() : v(-1) {}

  explicit GlobalId(int v) : v(v) {}

  explicit operator int() const {
    return v;
  }

  bool operator<(const GlobalId &id) const {
    return v < id.v;
  }

};

class LocalId {
protected:
  int v;

public:

  LocalId() : v(-1) {}

  explicit LocalId(int v) : v(v) {}

  explicit operator int() const {
    return v;
  }

  bool operator<(const LocalId &id) const {
    return v < id.v;
  }

};

struct Id {
  GlobalId globalId;
  LocalId localId;
};

class IndexMap {

protected:

  std::vector<GlobalId> locToglob;
  std::map<GlobalId, int> globToLoc;

public:

  size_t size() const {
    return locToglob.size();
  }

  int insert(const GlobalId &globalId);

  GlobalId localToGlobal(int locNum) const;

  int globalToLocal(const GlobalId &globalId) const;

  GlobalId operator()(int locNum) const {
    return localToGlobal(locNum);
  }

  int operator()(const GlobalId &globalId) const {
    return globalToLocal(globalId);
  }

};

#endif //MAPDL2AEROF_GLOBALID_H
