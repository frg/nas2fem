# python script for converting nastran op2 output file containing eigenvectors to xpost format 
# based on the following example:
# https://pynastran-git.readthedocs.io/en/latest/quick_start/op2_overview.html

import pyNastran
import os

op2_filename = 'modal_symm.op2'

from pyNastran.op2.op2 import OP2
model = OP2()
model.read_op2(op2_filename)

print(model.get_op2_stats())

itime = 0
isubcase = 1

disp = model.eigenvectors[isubcase]
nnodes = disp.data.shape[1]

fout = open('modal_symm.xpost', 'w')
msg = 'Vector MODE under Modal for nodeset\n'
fout.write(msg)
msg = '%d\n' % (nnodes)
fout.write(msg)

for itime in range(disp.data.shape[0]):

  # data = [tx, ty, tz, rx, ry, rz]
  # for some itime
  # all the nodes -> :
  # get [tx, ty, tz] -> :3
  txyz = disp.data[itime, :, :3]

  msg = '   %E\n' % disp.eigns[itime]
  fout.write(msg)
  for (nid, grid_type), txyzi in zip(disp.node_gridtype, txyz):
      #msg = ' %d % E % E % E\n' % (nid, txyzi[0], txyzi[1], txyzi[2])
      msg = ' % E % E % E\n' % (txyzi[0], txyzi[1], txyzi[2])
      fout.write(msg)

fout.close()
